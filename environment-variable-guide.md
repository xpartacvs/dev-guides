# Environment Variable Rules

[[_TOC_]]

## Variable Naming Rules

Bagian ini mengatur standard penulisan dan penamaan environment variable.

| **Directive**  | **Value**            | **Keterangan**                                                        | **Contoh**                                                                                           |
| :------------- | :------------------- | :-------------------------------------------------------------------- | :--------------------------------------------------------------------------------------------------- |
| Language       | English              | Harus menggunakan bahasa inggris                                      | `CREDENTIAL, PASSWORD`, dll                                                                          |
| Word Separator | Underscore (_)       | Harus menggunakan `_` (underscore) sebagai pemisah antar kata         | `APP_DEBUG, DB_HOST, DB_PASSWORD`                                                                    |
| Case           | UPPERCASE            | Harus huruf kapital                                                   | `CREDENTIAL`                                                                                         |
| Context Naming | Developer Idiomatic  | Sebisa mungkin gunakan istilah yang umum dikalangan developer.        | `URL, HOST, API, ID, dll`                                                                            |
| Length         | Shorter = Better     | Makin pendek makin baik namun harus tetap mudah dipahami oleh devops. | `DATABASE_USERNAME --> DB_USERNAME --> DB_USER` atau `DATABASE_PASSWORD --> DB_PASSWORD --> DB_PASS` |
| Plural         | On Array Type Only   | Gunakan kata bentuk jamak pada variabel bertipe array                 | `PROXIES, IPS, dll`                                                                                  |
| Order          | Alphabetic Ascending | Khusus kata pertama, urutkan sesuai abjad.                            | [Contoh dibawah](#alphabetic-ascending-and-contextual-prefix-example)                                |
| Grouping       | Contextual Prefix    | Kelompokkan environmen variable berdasarkan konteks.                  | [Contoh dibawah](#alphabetic-ascending-and-contextual-prefix-example)                                |

### Alphabetic Ascending and Contextual Prefix Example

```env
APP_NAME=Aplikasi
APP_ENV=development
APP_URL=http://localhost
APP_PORT=8080
DB_READ_HOST=db_master
DB_READ_PORT=3306
DB_READ_NAME=db_name
DB_READ_USER=master
DB_READ_PASS=passowrd
DB_WRITE_HOST=db_master
DB_WRITE_PORT=3306
DB_WRITE_NAME=db_name
DB_WRITE_USER=master
DB_WRITE_PASS=passowrd
JWT_SECRET=0123456789abcdef
PROXIES=172.0.0.1,172.0.1.0
```

- **Alphabetic Ascending**: Pada kata pertama nama variabel diurutkan sesuai abjad (`APP_, DB_, JWT_`).
- **Contextual Prefix**: Pada variabel yang terkait dengan pengaturan database, pengurutan variabel diatur berdasarkan peran database (READ/WRITE). `_HOST` dan `_PORT` diurutkan berdampingan karena terkati dengan koteks koneksi ke database.

## Documentation Rules

Bagian ini mengatur cara developer menuliskan dokumentasi list environment variable dengan maksud memudahkan tim devops untuk mengkonfigurasi CI/CD pipeline dan mendeploy aplkasi. Dokumentasi environment variable pada markdown harus disajikan dalam betuk tabel (dengan tetap mengikuti kaidah Alphabetic Ascending dan Contextual Prefix) dengan format seperti contoh dibawah ini:

| **Variable**         | **Type**    | **Required**       | **Default Value** | **Extra Info**            | **Description**                            |
| :---                 | :---        | :---:              | :---              | :---                      | :---                                       |
| `API_URL`            | `url`       | :bangbang:         |                   |                           | URL API utama                              |
| `APP_ENV`            | `enum`      |                    | `development`     | `development, production` | Mode environment Aplikasi                  |
| `APP_DEBUG`          | `boolean`   |                    | `true`            |                           | Mode debug Aplikasi                        |
| `ASSET_DIR`          | `path`      | :bangbang:         |                   | `relative`                | Path ke direktori asset aplikasi           |
| `CDN_HOSTS`          | `[host]`    | :bangbang:         |                   |                           | List hostname / IP server CDN              |
| `DB_HOST`            | `host`      | :bangbang:         |                   |                           | Hostname / IP server database              |
| `DB_PORT`            | `numeric`   |                    | `3306`            |                           | Port server database                       |
| `IP_WHITELIST`       | `cidr`      |                    | `0.0.0.0/0`       |                           | Whitelist IP yang boleh mengakses aplikasi |
| `JWT_SECRET`         | `string`    | :bangbang:         |                   |                           | Key enkripsi untuk token JWT               |
| `UPLOAD_DIR`         | `path`      | :bangbang:         |                   | `absolute`                | Path ke direktori tempat upload-an         |
| `WORDS_BLACKLIST`    | `[string]`  | :bangbang:         |                   |                           | List kata-kata terlarang                   |

- Variable yang **tidak required (optional)**, default value-nya harus diisi.
- Variable bertype `enum`, semua value yang tersedia harus dituliskan pada kolom **Extra Info**.
- Variable bertype `path`, tuliskan jenis pathnya apakah `absolute` atau `relative` pada kolom **Extra Info**.

## Type Rules

### string

- Tidak ada aturan khusus, hanya aturan string pada umumnya.

### numeric

- Tidak ada aturan khusus, hanya aturan numeric pada umumnya.

### boolean

- Satu-satunya aturan ialah value harus lowercase.

### enum

- Valuenya haruslah satu diantara list yang ditentukan oleh developer.

### host

- Value bisa berupa IP atau hostname yang mengacu pada suatu server tertentu.
- Contoh: `api.domain.tld` atau `127.0.0.1`

### path

- Value berisi path dari suatu directory atau file.
- Gunakan slash `/` sebagai directory separator.
- Tidak boleh ada trailing slash.
- Jika path absolute, harus ada leading slash. Contoh: `/mnt/nsf/sharedir`
- Jika path relative, tidak boleh ada leading slash. Contoh: `storage/image/logo`

### url

- Value harus ditulis dengan format `<scheme>://[[username]:[password]@]<host>[:port][/path]`
- Pada format diatas, `<>` bersifat required.
- Pada format diatas, `[]` bersifat optional.
- Tidak boleh ada trailing slash.
- Contoh: `https://domain.tld/api/v1`

### cidr

- Value harus ditulis dengan format [Classless Inter-Domain Routing (CIDR)](https://www.keycdn.com/support/what-is-cidr)
- Contoh: `172.16.0.0/24`, dimana ini artinya adalah list IP dengan range 172.16.0.0 s/d 172.16.0.255

### list/array

- Hanya satu dimensi.
- Gunakan `,` (koma) sebagai sepatator antar elemen.
- Pada [dokumentasi](#documentation-rules), notasikan dengan `[<type>]` jika tipe elemen yang sejenis. Contoh: `[string]`.
- Pada [dokumentasi](#documentation-rules), notasikan dengan `[mix]` jika tipe elemen beragam.
